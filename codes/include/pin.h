#ifndef PIN_H
#define PIN_H

#include "stm32f0xx.h"
#include "stm32f0xx_conf.h"

void config_pin();
void config_push_button();
void config_LEDs();
void config_interrupt();

#endif
