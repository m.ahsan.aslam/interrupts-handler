/*
 * pin.c
 *
 *  Created on: 30.01.2019
 *      Author: jf230
 */
#include "pin.h"

//------------------------------------------------------------------------------
void config_pin()
{
	config_push_button();
	config_LEDs();
	config_interrupt();
}
//------------------------------------------------------------------------------
void config_push_button()
{

	GPIO_InitTypeDef GPIO_InitStruct;

	//Clock enabling
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	GPIO_StructInit(&GPIO_InitStruct);
	//GPIO PortA 10 connected with button
	GPIO_InitStruct.GPIO_Pin  = GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;

	//GPIOx includes portA, portB, portC, portD, PortE and portF
	GPIO_Init(GPIOA, &GPIO_InitStruct);
}
//------------------------------------------------------------------------------
void config_LEDs()
{

	GPIO_InitTypeDef GPIO_InitStruct;

	//Clock enabling
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	GPIO_StructInit(&GPIO_InitStruct);
	//GPIO PortB 0,1,2,3 connected with LED
	GPIO_InitStruct.GPIO_Pin  = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;

	//GPIOx includes portA, portB, portC, portD, PortE and portF
	GPIO_Init(GPIOB, &GPIO_InitStruct);
}
//------------------------------------------------------------------------------
void config_interrupt()
{
	EXTI_InitTypeDef EXTI_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource10);
	EXTI_StructInit(&EXTI_InitStruct);
	EXTI_InitStruct.EXTI_Line = EXTI_Line10;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI4_15_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
}
//------------------------------------------------------------------------------
